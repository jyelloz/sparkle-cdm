#pragma once

#include <string>
#include <optional>
#include <span>
#include <unordered_map>

#include "open_cdm.h"
#include "content_decryption_module.h"

using std::optional;
using std::string;
using std::span;
using std::unordered_map;

struct OpenCDMSession {
  OpenCDMSession(
      string id,
      cdm::SessionType sessionType,
      OpenCDMSystem* system,
      OpenCDMSessionCallbacks* callbacks,
      void* userData
  );
  ~OpenCDMSession() = default;

  void errorCallback(const string& message);
  void licenseRequestCallback(span<const uint8_t> message);
  void licenseRenewalCallback(span<const uint8_t> message);
  void licenseReleaseCallback(span<const uint8_t> message);
  void individualizationRequestCallback(span<const uint8_t> message);

  void onKeyUpdate(span<const cdm::KeyInformation> keys);
  optional<cdm::KeyInformation> getKeyInfo(const string& keyId) const;
  bool hasKey(const string& keyId) const;

  string id;
  cdm::SessionType sessionType;
  cdm::Time expiration;
  OpenCDMSystem* system;
  OpenCDMSessionCallbacks* callbacks;
  void* userData;
  unordered_map<string, cdm::KeyInformation> keyInfo;
};
